# node-virtual-input

This nodejs lib lets you simulate keypresses, gamepad controls, joystick controls, mouse movement and clicks; all _*natively*_ using ```uinput```.

##Prerequisite
------------
This is only compatible with Linux OS with the **uinput** kernel module installed.

##Installation
------------
```
    npm install --save virtual-input
```

##Usage
-----
```
const Input = require('virtual-input');

let opts = {};
let input = new Input(opts);
```

##Methods
--------

```
.connect(callback)
```
Connects input device to device.

```
.disconnect(callback)
```
Disconnects input device from device.

```
.sendEvent({type: <EVENTTYPE>, code: <EVENTCODE>, value: <VALUE>})
```
Sends an event.

```type``` and ```code``` are numbers based on [the event types](https://www.kernel.org/doc/Documentation/input/event-codes.txt) and [the event codes](http://lxr.free-electrons.com/source/include/uapi/linux/input-event-codes.h).
Some of these types and codes are included in ```lib/uinput.js```, if ```type``` and ```code``` are given strings, their value will be taken from there. (not all codes exist there).

##Options
--------
```
opts.ioctlArray
```
You may supply an array of two array pairs of ioctl 'features'.

```
let input = new Input({ioctlArray: [
	['UI_SET_KEYBIT', 'BTN_MIDDLE'],
	['UI_SET_KEYBIT', 'BTN_EXTRA']
]});
```

##Examples
----------

There is ```rectangle.js``` in examples dir. If you want real code samples, then you can check out my sphero-as-joystick repo.

Forked and Worked on [node-virtual-gamepads](https://github.com/miroof/node-virtual-gamepads)
