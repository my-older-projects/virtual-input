'use strict'; //eslint-disable-line
// Simplified

let Gamepad = require('./virtual_gamepad');

module.exports = class {
	constructor(opts) {
		opts = opts || {};
		this.ioctlArray = opts.ioctlArray;
	}

	connect(callback) {
		this.gamepad = new Gamepad(this.ioctlArray);
		return this.gamepad.connect(callback, function(err) {
			return callback(err);
		});
	}

	disconnect(callback) {
		if (this.gamepad) {
			return this.gamepad.disconnect(callback);
		}
		return 0;
	}

	sendEvent(event) {
		if (this.gamepad) {
			return this.gamepad.sendEvent(event);
		}
		return 0;
	}
};
