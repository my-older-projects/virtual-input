'use strict';
const Input = require('../');
let input = new Input();
const delay = 5;

const a = 400;
const b = 400;

let line = (A, times, step, cb, index) => {
	index = index || 0;

	if (index === times) {
		return cb();
	}

	return setTimeout(() => {
		input.sendEvent({type: 'EV_REL', code: 'REL_'.concat(A), value: step});
		return line(A, times, step, cb, index + 1);
	}, delay);
};

let square = () => {
	line('X', a, 1, () => {
		line('Y', b, 1, () => {
			line('X', a, -1, () => {
				line('Y', b, -1, () => {
					square();
				});
			});
		});
	});
};

input.connect(() => {
	square();
});
