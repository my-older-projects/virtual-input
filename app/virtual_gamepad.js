'use strict';
/*
Created by MIROOF on 04/03/2015
Virtual gamepad class
 */
const fs = require('fs');
const ioctl = require('ioctl');
const uinput = require('../lib/uinput');
const ref = require('ref');
const arrayType = require('ref-array');
const structType = require('ref-struct');

/*	struct input_id {
		_u16 bustype;
		_u16 vendor;
		_u16 product;
		_u16 version;
	}; */
const InputId = structType({
	bustype: ref.types.uint16,
	vendor: ref.types.uint16,
	product: ref.types.uint16,
	version: ref.types.uint16
});
/*  struct uinput_user_dev {
		char name[UINPUT_MAX_NAME_SIZE];
		struct input_id id;
			int ff_effects_max;
			int absmax[ABS_MAX + 1];
			int absmin[ABS_MAX + 1];
			int absfuzz[ABS_MAX + 1];
			int absflat[ABS_MAX + 1];
	}; */
const DeviceName = arrayType(ref.types.char, uinput.UINPUT_MAX_NAME_SIZE);
const AbsArray = arrayType(ref.types.int, uinput.ABS_CNT);
const UInputUserDev = structType({
	name: DeviceName,
	id: InputId,
	ff_effects_max: ref.types.int,
	absmax: AbsArray,
	absmin: AbsArray,
	absfuzz: AbsArray,
	absflat: AbsArray
});

module.exports = class {
	constructor(inputArr) {
		this.iArr = inputArr || [];
	}

	connect(callback) {
		let fd = this.fd = fs.openSync('/dev/uinput', 'w+');

		let name = 'spheroJoy';
		let bustype = uinput.BUS_USB;
		let vendor = 0x0102;
		let product = 0x2000;
		let version = 1;

		let nM = (new Buffer(new Array(80)));
		nM.write(name);
		let absmax = new Array(uinput.ABS_CNT).fill(0);
		absmax[uinput.ABS_X] = 255;
		absmax[uinput.ABS_Y] = 255;
		let absmin = new Array(uinput.ABS_CNT).fill(0);
		absmin[uinput.ABS_X] = 0;
		absmin[uinput.ABS_Y] = 0;
		let absflat = new Array(uinput.ABS_CNT).fill(0);
		absflat[uinput.ABS_X] = 15;
		absflat[uinput.ABS_Y] = 15;
		let absfuzz = new Array(uinput.ABS_CNT).fill(0);
		absfuzz[uinput.ABS_X] = 0;
		absfuzz[uinput.ABS_Y] = 0;

		let uIConf = new UInputUserDev({
			name: new DeviceName(nM),
			id: {
				bustype,
				vendor,
				product,
				version
			},
			ff_effects_max: 0,
			absmax: new AbsArray(absmax),
			absmin: new AbsArray(absmin),
			absfuzz: new AbsArray(absfuzz),
			absflat: new AbsArray(absflat)
		});

		let buff = uIConf.ref();
		fs.writeSync(fd, buff, 0, buff.length, null);

		[['UI_SET_EVBIT', 'EV_KEY'],
		['UI_SET_KEYBIT', 'BTN_LEFT'],
		['UI_SET_KEYBIT', 'BTN_RIGHT'],
		['UI_SET_EVBIT', 'EV_REL'],
		['UI_SET_RELBIT', 'REL_X'],
		['UI_SET_RELBIT', 'REL_Y'],
		['UI_SET_RELBIT', 'REL_WHEEL'],
		['UI_SET_EVBIT', 'EV_KEY'],
		['UI_SET_KEYBIT', 'BTN_A'],
		['UI_SET_KEYBIT', 'BTN_B'],
		['UI_SET_KEYBIT', 'BTN_X'],
		['UI_SET_KEYBIT', 'BTN_Y'],
		['UI_SET_KEYBIT', 'BTN_TL'],
		['UI_SET_KEYBIT', 'BTN_TR'],
		['UI_SET_KEYBIT', 'BTN_START'],
		['UI_SET_KEYBIT', 'BTN_SELECT'],
		['UI_SET_EVBIT', 'EV_ABS'],
		['UI_SET_ABSBIT', 'ABS_X'],
		['UI_SET_ABSBIT', 'ABS_Y']].concat(this.iArr).forEach(function(data) {
			let erste = uinput[data[0]];
			let zweite = uinput[data[1]];
			ioctl(fd, erste, zweite);
		});

		let addKeys = i => {
			if (i === 120) {
				return 0;
			}
			ioctl(fd, uinput.UI_SET_KEYBIT, i);
			return addKeys(i + 1);
		};
		addKeys(1);

		ioctl(fd, uinput.UI_DEV_CREATE, 0);

		return callback();
	}

	disconnect(callback) {
		if (this.fd) {
			ioctl(this.fd, uinput.UI_DEV_DESTROY, 0);
			fs.close(this.fd);
			this.fd = undefined;
			return callback();
		}

		return this;
	}

	sendEvent(event) {
		var ev = new Buffer(24);
		ev.fill(0);

		let tvSec = Math.round(Date.now() / 1000);
		let tvUsec = Math.round(Date.now() % 1000 * 1000);
		let type;
		if (typeof event.type === 'string') {
			type = uinput[event.type];
		} else {
			type = event.type;
		}
		let code;
		if (typeof event.code === 'string') {
			code = uinput[event.code];
		} else {
			code = event.code;
		}
		let value = event.value;

		ev.writeInt32LE(tvSec, 0);
		ev.writeInt32LE(tvUsec, 8);
		ev.writeInt16LE(type, 16);
		ev.writeInt16LE(code, 18);
		ev.writeInt32LE(value, 20);

		let evEnd = new Buffer(24);
		evEnd.fill(0);

		evEnd.writeInt32LE(tvSec, 0);
		evEnd.writeInt32LE(tvUsec, 8);

		fs.writeSync(this.fd, ev, 0, ev.length, null);
		fs.writeSync(this.fd, evEnd, 0, evEnd.length, null);
	}
};
